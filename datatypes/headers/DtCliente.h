#ifndef DTCLIENTE_H
#define DTCLIENTE_H
#include <iostream>
class DtCliente
{

private:
	std::string nombre;
	int telefono;
	std::string direccion;

public:

	DtCliente(std::string nombre, int telefono, std::string direccion);

	std::string getNombre();
	int getTelefono();
	std::string getDireccion();

	void setNombre(std::string nombre);
	void setTelefono(int telefono);
	void setDireccion(std::string direccion);

	virtual ~DtCliente();


};

#endif
