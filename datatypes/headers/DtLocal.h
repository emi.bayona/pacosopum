#ifndef DTLOCAL_H
#define DTLOCAL_H
#include "DtVenta.h"

class DtLocal : public DtVenta{


	private:

	public: 

		DtLocal(int numeroVenta, float total, float subtotal, int descuento);
		
		virtual ~DtLocal();

};

#endif