#ifndef DTFACTURA_H
#define DTFACTURA_H
#include "DtFecha.h"

class DtFactura{

	private:
		int codigoVenta;
		DtFecha fecha;

	public:

		DtFactura(int codigoVenta, DtFecha fecha);

		int getCodigoVenta();

		void setCodigoVenta(int codigoVenta);

		virtual ~DtFactura();

};

#endif