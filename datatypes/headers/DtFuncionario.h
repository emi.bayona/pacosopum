#ifndef DTFUNCIONARIO_H
#define DTFUNCIONARIO_H
#include <iostream>


class DtFuncionario{

	protected:

		int numero;
		std::string nombre;

	public:

		DtFuncionario(int numero, std::string nombre);

		int getNumero();
		std::string getNombre();

		void setNumero(int numero);
		void setNombre(std::string nombre);

		virtual ~DtFuncionario();



};

#endif
