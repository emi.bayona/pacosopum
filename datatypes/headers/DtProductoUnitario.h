#ifndef DTPRODUCTOUNITARIO_H
#define DTPRODUCTOUNITARIO_H
#include "../headers/DtProducto.h"

#include <iostream>

class DtProductoUnitario : public DtProducto{

	private:

	public:	

		DtProductoUnitario(std::string codigo, std::string descripcion, float precio);

		virtual ~DtProductoUnitario();
		
};

#endif
