#ifndef DTPRODUCTO_H 
#define DTPRODUCTO_H
#include <iostream>

class DtProducto{

	protected:

		std::string codigo;
		std::string descripcion;
		float precio;

	public:

		DtProducto(std::string codigo, std::string descripcion, float precio);

		std::string getCodigo();
		std::string getDescripcion();
		float getPrecio();

		void setCodigo(std::string codigo);
		void setDescripcion(std::string descripcion);
		void setPrecio(float precio);

		virtual ~DtProducto();
};

#endif
