#ifndef DTDOMICILIO_H
#define DTDOMICILIO_H

#include "../headers/DtVenta.h"


class DtDomicilio : public DtVenta{

	private:

	public: 

		DtDomicilio(int numeroVenta, float total, float subtotal, int descuento);
		
		virtual ~DtDomicilio();

};

#endif
