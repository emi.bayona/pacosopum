#ifndef DTMESA_H 
#define DTMESA_H

class DtMesa{

	private:

		int numeroMesa;

	public:

		DtMesa(int numeroMesa);

		int getNumeroMesa();

		void setNumeroMesa(int numeroMesa);

		virtual ~DtMesa();


};

#endif
