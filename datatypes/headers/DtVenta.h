#ifndef DTVENTA_H
#define DTVENTA_H


class DtVenta{

	protected:
		int numeroVenta;
		float total;
		float subTotal;
		int descuento;

	public:

		DtVenta(int numeroVenta, float total, float subTotal, int descuento);

		int getNumeroVenta();
		float getTotal();
		float getSubTotal();
		int getDescuento();

		void setNumeroVenta(int numeroVenta);
		void setTotal(float total);
		void setSubTotal(float subTotal);
		void setDescuento(int descuento);

		virtual ~DtVenta();

};

#endif
