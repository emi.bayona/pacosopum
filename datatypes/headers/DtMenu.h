#ifndef DTMENU_H
#define DTMENU_H
#include "DtProducto.h"
#include <iostream>

class DtMenu : public DtProducto{


	private:

		std::string nombre;

	public:
		
		DtMenu(std::string nombre, std::string codigo, std::string descripcion, float precio);

		std::string getNombreMenu();

		void setNombreMenu(std::string nombre);

		virtual ~DtMenu();

};

#endif
