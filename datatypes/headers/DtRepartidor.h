#ifndef DTREPARTIDOR_H 
#define DTREPARTIDOR_H
#include "DtFuncionario.h"
#include <iostream>

class DtRepartidor : public DtFuncionario{


	public:
		enum TRAN{Pie,Bicicleta,Motocicleta};

		DtRepartidor(TRAN transporte, int numero, std::string nombre);

		TRAN getTransporte();
		void setTransporte(TRAN transporte);
		virtual ~DtRepartidor();

	private:
		TRAN transporte;
};

#endif
