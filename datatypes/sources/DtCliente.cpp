#include "../headers/DtCliente.h"

DtCliente::DtCliente(std::string nombre, int telefono, std::string direccion){

	this->nombre=nombre;
	this->telefono=telefono;
	this->direccion=direccion;

}

std::string DtCliente::getNombre(){
	return this->nombre;
}

int DtCliente::getTelefono(){
	return this->telefono;
}

std::string DtCliente::getDireccion(){
	return this->direccion;

}

void DtCliente::setNombre(std::string nombre){
	this->nombre=nombre;

}

void DtCliente::setTelefono(int telefono){
	this->telefono=telefono;
}

void DtCliente::setDireccion(std::string direccion){
	this->direccion=direccion;
}

DtCliente::~DtCliente(){

}

