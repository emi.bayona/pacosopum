#include "../headers/DtProducto.h"


DtProducto::DtProducto(std::string codigo, std::string descripcion, float precio){
	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;
}

std::string DtProducto::getCodigo(){
	return this->codigo;
}

std::string DtProducto::getDescripcion(){
	return this->descripcion;
}

float DtProducto::getPrecio(){
	return this->precio;
}

void DtProducto::setCodigo(std::string codigo){

	this->codigo=codigo;
}

void DtProducto::setDescripcion(std::string descripcion){

	this->descripcion=descripcion;
}

void DtProducto::setPrecio(float precio){

	this->precio=precio;
}

DtProducto::~DtProducto(){

}
