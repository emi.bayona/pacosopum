#include "../headers/DtFuncionario.h"

DtFuncionario::DtFuncionario(int numero, std::string nombre){
	this->numero=numero;
	this->nombre=nombre;

}

int DtFuncionario::getNumero(){
	return this->numero;

}

std::string DtFuncionario::getNombre(){
	return this->nombre;	
}

void DtFuncionario::setNumero(int numero){
	this->numero=numero;
}

void DtFuncionario::setNombre(std::string nombre){
	this->nombre=nombre;

}

DtFuncionario::~DtFuncionario(){

}

