#include "../headers/DtMenu.h"

DtMenu::DtMenu(std::string nombre, std::string codigo, std::string descripcion, float precio):DtProducto(codigo,descripcion,precio){

	this->nombre=nombre;
	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;
}

std::string DtMenu::getNombreMenu(){
	return this->nombre;
}

void DtMenu::setNombreMenu(std::string nombre){

	this->nombre=nombre;

}

DtMenu::~DtMenu(){

}
