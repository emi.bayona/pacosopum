#include "../headers/DtVenta.h"


DtVenta::DtVenta(int numeroVenta, float total, float subTotal, int descuento){

	this->numeroVenta=numeroVenta;
	this->total=total;
	this->subTotal=subTotal;
	this->descuento=descuento;
}

int DtVenta::getNumeroVenta(){
	return this->numeroVenta;
}

float DtVenta::getTotal(){
	return this->total;
}

float DtVenta::getSubTotal(){
	return this->subTotal;
}

int DtVenta::getDescuento(){
	return this->descuento;
}

void DtVenta::setNumeroVenta(int numeroVenta){

	this->numeroVenta=numeroVenta;
}

void DtVenta::setTotal(float total){

	this->total=total;
}

void DtVenta::setSubTotal(float subTotal){

	this->subTotal=subTotal;
}

void DtVenta::setDescuento(int descuento){

	this->descuento=descuento;
}

DtVenta::~DtVenta(){

}
