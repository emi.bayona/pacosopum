#include "../headers/DtRepartidor.h"


DtRepartidor::DtRepartidor(DtRepartidor::TRAN transporte, int numero, std::string nombre):DtFuncionario(numero,nombre){

	this->transporte=transporte;
	this->numero=numero;
	this->nombre=nombre;

}

DtRepartidor::TRAN DtRepartidor::getTransporte(){
	return this->transporte;

}

void DtRepartidor::setTransporte(TRAN transporte){

	this->transporte=transporte;
}

DtRepartidor::~DtRepartidor(){

}
