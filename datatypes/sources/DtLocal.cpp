#include "../headers/DtVenta.h"
#include "../headers/DtLocal.h"

DtLocal::DtLocal(int numeroVenta, float total, float subTotal, int descuento):DtVenta(numeroVenta,total,subTotal,descuento){
	
	this->numeroVenta=numeroVenta;
	this->total=total;
	this->subTotal=subTotal;
	this->descuento=descuento;
}

DtLocal::~DtLocal(){
	
}