#include "../headers/DtProductoUnitario.h"

DtProductoUnitario::DtProductoUnitario(std::string codigo, std::string descripcion, float precio):DtProducto(codigo,descripcion,precio){

	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;
}

DtProductoUnitario::~DtProductoUnitario(){

}
