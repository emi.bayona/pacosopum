#ifndef CONTIENE_H
#define CONTIENE_H
#include "Producto.h"
#include "../../classes/headers/Menu.h"
#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"
#include <iostream>


class Contiene
{
	private: 
		IDictionary* productos;
		IDictionary* cantProductos;

	public:
		//Contiene(int cantidad, ProductoUnitario* prodU);
		Contiene();
		// Getters 
		
		//estan ordenados con la misma {KEY},
		IDictionary* getProdContiene();
		IDictionary* getCantContiene();
		

		// Setters 
		void addProductosContiene(ICollection* cants, IDictionary* prodUni);
		void addProducto(Producto *prod, int cant);


		virtual ~Contiene();



};
#endif
