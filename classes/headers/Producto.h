#ifndef PRODUCTO_H
#define PRODUCTO_H
#include "Transaccion.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"

class Transaccion;

class Producto:public ICollectible
{
	protected: 
		string codigo;
		string descripcion;
		float precio;

		Transaccion** transaccion;
		
	public:
		Producto(string codigo, string descripcion, float precio);

		// Getters 
		std::string getCodigo();
		std::string getDescripcion();
		float getPrecio();

		// Setters 
		void setCodigo(string codigo);
		void setDescripcion(string descripcion);
		void setPrecio(float precio);

		virtual ~Producto();




};
#endif
