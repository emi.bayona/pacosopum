#ifndef DOMICILIO_H
#define DOMICILIO_H
#include "../../classes/headers/Venta.h"

class Cliente;
class Repartidor;

class Domicilio : public Venta
{
	private: 

		Cliente** cliente;
		Repartidor** repartidor;

	public:

		// Constructor
		Domicilio(int numero, float total, float subtotal, int descuento);

		virtual ~Domicilio();



};
#endif
