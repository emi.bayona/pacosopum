#ifndef FACTURA_H
#define FACTURA_H
#include "../../datatypes/headers/DtFecha.h"
#include "../../datatypes/headers/DtHora.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"

class Venta;
class Factura:public ICollectible{
	private: 
		int codigoVenta;
		DtFecha fecha;
		DtHora hora;

		Factura** factura;
		
	public:
		Factura(int codigoVenta, DtFecha fecha, DtHora hora);

		// Getters 
		int getCodigoVenta();
		DtFecha getFecha();
		DtHora getHora();

		// Setters 
		void setCodigoVenta(int codigoVenta);
		void setFecha(DtFecha fecha);
		void setHora(DtHora hora);

		virtual ~Factura();



};
#endif

