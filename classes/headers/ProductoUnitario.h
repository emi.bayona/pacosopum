#ifndef PRODUCTOUNITARIO_H
#define PRODUCTOUNITARIO_H
#include "Contiene.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"

class Contiene;

class ProductoUnitario : public Producto
{
	private: 
		string codigo;
		string descripcion;
		float precio;
		
		Contiene** contiene;

	public:

		// Constructor
		ProductoUnitario(string codigo, string descripcion, float precio);
		static void listarProductosUnitarios(IDictionary* dicProductosU);
		
		virtual ~ProductoUnitario();



};
#endif
