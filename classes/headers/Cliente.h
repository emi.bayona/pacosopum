
#ifndef CLIENTE_H
#define CLIENTE_H
#include "../../classes/headers/Domicilio.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"


class Cliente:public ICollectible{
	private: 
		string nombre;
		int telefono;
		string direccion;

//		Domicilio* domicilio;

	public:
		Cliente(string nombre, int telefono, string direccion);

		// Getters 
		string getNombre();
		int getTelefono();
		string getDireccion();

		// Setters 
		void setNombre(string nombre);
		void setTelefono(int telefono);
		void setDireccion(string direccion);
		
		virtual ~Cliente();
};
#endif
