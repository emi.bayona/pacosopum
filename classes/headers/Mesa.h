#ifndef MESA_H
#define MESA_H

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"

#include "Mozo.h"
#include "Local.h"

class Mozo;

class Mesa:public ICollectible{

	private: 
		int numero;
	
		Mozo** mozo;
		Local** local;
		
	public:
		Mesa(int numero);

		// Getters 
		int getNumero();

		// Setters 
		void setNumero(int numero);
		
		
		virtual ~Mesa();



};
#endif
