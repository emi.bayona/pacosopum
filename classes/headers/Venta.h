#ifndef VENTA_H
#define VENTA_H
#include "Factura.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/collections/List.h"
#include "../../ICollection/interfaces/ICollectible.h"
#include "../../ICollection/interfaces/ICollection.h"

class Factura;
class Transaccion;

class Venta:public ICollectible
{


	protected: 
		int numero;
		float total;
		float subtotal;
		int descuento;

		ICollection* transacciones;

		Transaccion** transaccion;
		Factura** factura;

	public:
		Venta(int numero, float total, float subtotal, int descuento);

		// Getters 
		int getNumero();
		float getTotal();
		float getSubtotal();
		int getDescuento();

		ICollection* getTransacciones();

		// Setters 

		void addTransacciones(ICollection* tran);

		void addTransaccion(ICollectible *tran);
		void setNumero(int numero);
		void setTotal(float total);
		void setSubtotal(float subtotal);
		void setDescuento(int descuento);

		virtual ~Venta();



};
#endif
