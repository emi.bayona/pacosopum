#ifndef MENU_H
#define MENU_H
#include "Contiene.h"
#include "Producto.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/Integer.h"
#include "../../ICollection/interfaces/ICollectible.h"
#include "ProductoUnitario.h"
#include <iostream>

class Contiene;

class Menu : public Producto{
	private: 
		string nombre;
		
		Contiene* contiene;
		
	public:

		// const { return ClaseA::LeerValor(); }
		// Constructor
		Menu(std::string codigo, std::string descripcion, float precio, std::string nombre);
		static void listarMenuses(IDictionary* dicMenus);
		// Getters 
		string getNombre();
		Contiene* getContiene();

		// Setters 
		void setNombre(std::string nombre);
		void setContiene(Contiene* contiene);

		//

		virtual ~Menu();



};
#endif
