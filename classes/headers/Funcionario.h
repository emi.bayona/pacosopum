#ifndef FUNCIONARIO_H
#define FUNCIONARIO_H

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollectible.h"

class Funcionario:public ICollectible
{
	protected: 
		int numero;
		string nombre;

	public:
		Funcionario(int numero, std::string nombre);

		// Getters 
		int getNumero();
		std::string getNombre();

		// Setters 
		void setNumero(int numero);
		void setNombre(std::string nombre);
		
		virtual ~Funcionario();



};
#endif
