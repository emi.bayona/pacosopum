#ifndef TRANSACCION_H
#define TRANSACCION_H
#include "Producto.h"
#include "Venta.h"


#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/StringKey.h"
#include "../../ICollection/Integer.h"
#include "../../ICollection/interfaces/ICollectible.h"

class Producto;
class Venta;


class Transaccion:public ICollectible
{
	private: 

		IDictionary* productos;
		IDictionary* cantProductos;

		Producto** prod;
		Venta** venta;

	public:
		Transaccion();

		// Getters 
	
		IDictionary* getProdTransaccion();
		IDictionary* getCantTransaccion();

		void addProductosTransaccion(ICollection* cants, IDictionary* prod);
		void addProducto(Producto* prod, int cant);
		// Setters 
		
		
		virtual ~Transaccion();



};
#endif
