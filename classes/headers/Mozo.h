#ifndef MOZO_H
#define MOZO_H
#include "Mesa.h"
#include "Local.h"
#include "Funcionario.h"

#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/IntKey.h"
#include "../../ICollection/interfaces/ICollectible.h"



class Mesa;
class Local;

class Mozo : public Funcionario
{
	
	private: 
		//vars
		int numero;
		string nombre;
		IDictionary* dicMesas;

		Local** local;
		
	public:

		// Constructor
		Mozo(int numero, std::string nombre);

		int getNumero();
		IDictionary* getMesas();
		std::string getNombre();


		void setNumero(int numero);
		void setNombre(std::string nombre);
		void addMesa(Mesa* m);

		

	virtual ~Mozo();


};
#endif
