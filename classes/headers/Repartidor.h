#ifndef REPARTIDOR_H
#define REPARTIDOR_H
#include "Domicilio.h"
#include "Funcionario.h"

class Domicilio;

class Repartidor : public Funcionario
{
	
	public:
		enum TRAN{Pie,Bicicleta,Motocicleta};

		// Constructor
		Repartidor(int numero, std::string nombre, TRAN transporte);

		// Getters 
		TRAN getTransporte();

		// Setters 
		void setTransporte(TRAN transporte);
		
		
	private: 
		TRAN transporte;
		Domicilio** domicilio;

	virtual ~Repartidor();
};

#endif
