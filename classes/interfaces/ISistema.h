#ifndef ISISTEMA_H
#define ISISTEMA_H

#include "../../ICollection/interfaces/ICollection.h"
#include "../../ICollection/interfaces/IDictionary.h"
#include "../../datatypes/headers/DtDomicilio.h"
#include "../../datatypes/headers/DtFecha.h"
#include "../../datatypes/headers/DtHora.h"
#include "../../datatypes/headers/DtCliente.h"
#include "../../datatypes/headers/DtMesa.h"
#include "../../datatypes/headers/DtFuncionario.h"
#include "../../datatypes/headers/DtProducto.h"
#include "../../datatypes/headers/DtFactura.h"
#include "../../datatypes/headers/DtVenta.h"



class ISistema {
    public:
        ISistema();
        
        virtual DtMesa* iniciarVentas(int idMozo) = 0;
        virtual DtMesa seleccionarMesa(int numeroMesa) = 0;
        virtual void agregarVentaLocal(int numeroMesa) = 0;
        virtual void aplicarDescuento (int descuento) = 0;
        virtual DtFactura obtenerFactura (int codigoVenta) = 0;
        virtual DtFecha* getSystemDate() = 0;
        virtual void darBajaProducto(std::string codigo) = 0;
        virtual void darBajaMenu (std::string codigo, string nombre) = 0;
        virtual void agregarProductoMenu(std::string codigo, std::string descuento) = 0;
        virtual void listarProductosComunes() = 0;
        virtual DtProducto seleccionarProducto (std::string id, std::string cantidad) = 0;
        virtual void agregarProductoComun(std::string id, std::string descuento, float precio) = 0;
        virtual void altaProducto(std::string codigo, std::string desc, float precio) = 0;
        virtual void eliminarProducto() = 0;
        virtual void decrementarProducto () = 0; //wat?
        virtual void eliminarMenu() = 0;

};

#endif /* ISISTEMA_H */

