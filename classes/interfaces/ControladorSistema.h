#ifndef CONTROLADORSISTEMA_H
#define CONTROLADORSISTEMA_H
#include "../../ICollection/StringKey.h"
#include "../../ICollection/interfaces/ICollection.h"
#include "../../ICollection/collections/List.h"
#include "../../ICollection/interfaces/IDictionary.h"
#include "../../ICollection/collections/OrderedDictionary.h"
#include "../../ICollection/collections/List.h"
#include "../../ICollection/IntKey.h"
#include "../../datatypes/headers/DtDomicilio.h"
#include "../headers/Cliente.h"
#include "../headers/Contiene.h"
#include "../headers/Domicilio.h"
#include "../headers/Factura.h"
#include "../headers/Funcionario.h"
#include "../headers/Local.h"
#include "../headers/Menu.h"
#include "../headers/Mesa.h"
#include "../headers/Mozo.h"
#include "../headers/Producto.h"
#include "../headers/ProductoUnitario.h"
#include "../headers/Repartidor.h"
#include "../headers/Transaccion.h"
#include "../headers/Venta.h"
#include "../interfaces/ISistema.h"
#include <ctime>



class ControladorSistema : public ISistema {
    private:
        ICollection* colFacturas;
        ICollection* colTransacciones;
        ICollection* colContiene;
        IDictionary* dicVentas;
        IDictionary* dicProductosU;
        IDictionary* dicMenus;
        IDictionary* dicFuncionarios;
        IDictionary* dicMesas;
        IDictionary* dicMozos;
        IDictionary* dicMesasVenta;
     
       // Usuario *sesion;
        DtHora* horaSystema;
        DtFecha* fechaSystema;
    public:
        ControladorSistema();
        /***************** */
        DtHora* getSystemHour();
        DtFecha* getSystemDate();
        /* ----------------------- */
        // Separar por grupos ndea

        DtMesa* iniciarVentas(int idMozo);
        DtMesa seleccionarMesa(int numeroMesa);
        void agregarVentaLocal(int numeroMesa);
        void aplicarDescuento (int descuento);
        DtFactura obtenerFactura (int codigoVenta);
        void listarProductosComunes();
        void listarProductoMenu();
        DtProducto seleccionarProducto (std::string id, std::string cantidad);
        void agregarProductoMenu(std::string codigo, int cantidad);
        void agregarProductoComun(std::string id, std::string descripcion, float precio);
        void altaProducto(std::string codigo, std::string desc, float precio); //falta recibir algo
        bool hayProductos();
        //Secction Eliminatorias
        void eliminarProductoVenta(std::string codigoP, int cant, std::string codigoV);
        void darBajaMenu(std::string codigo, string nombre);
        void darBajaProducto(std::string codigo);
        void eliminarProductoMenu(std::string codigoP, std::string codigoM);


        ~ControladorSistema();
};

#endif /* CONTROLADORSISTEMA_H */
