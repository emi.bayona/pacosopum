#include "ControladorSistema.h"

		ControladorSistema::ControladorSistema(){
			colFacturas = new List();
			colTransacciones = new List();
			colContiene = new List();
			dicVentas = new OrderedDictionary();
			dicProductosU = new OrderedDictionary();
			dicMenus = new OrderedDictionary();
			dicFuncionarios = new OrderedDictionary();
			dicMesas = new OrderedDictionary();
            dicMozos = new OrderedDictionary();
            dicMesasVenta = new OrderedDictionary();

		    time_t now = time(0);
		    tm *ltm = localtime(&now);
		    horaSystema = new DtHora(ltm->tm_hour,ltm->tm_min);
		    fechaSystema = new DtFecha(ltm->tm_mday,ltm->tm_mon,ltm->tm_year+1900);
		}
        /***************** */
        DtHora* ControladorSistema::getSystemHour(){
    		return this->horaSystema;
		}

		DtFecha* ControladorSistema::getSystemDate(){
		    return this->fechaSystema;
		}
		
        /* ----------------------- */
        // Separar por grupos ndea

         DtMesa* ControladorSistema::iniciarVentas(int idMozo){
            //Busco las mesas que tiene asignada un mozo
            IKey *k_m = new Integer(idMozo);
            Mozo *mozo = dynamic_cast<Mozo*>(dicMozos->find(k_m));
                if(mozo == nullptr){//cambio
                    throw std::invalid_argument(" El mozo no tiene mesas asignadas");
                }else{
            //obtengo las mesas que tiene ese mozo
                IDictionary *mesas = mozo->getMesas();
                return mesas;
                }

          

         }

         DtMesa ControladorSistema::seleccionarMesa(DtMesa mesas){
                //Tengo el diccionario de mesas disponibles para el mozo,
                // Ahora tengo que guardar en ese diccionario las mesas que selecciona
            //Por cada mesa disponible preguntar para agregar a la venta
                int mesita;
                std::cout<< "     Ingrese el numero de mesa: "
                //recibo el numero de mesa y lo guardo en mesita
                std::cin>> mesita;


                IKey *k_mesita = new Integer(mesita);
                IDictionary mesas = dynamic_cast<Mozo*>(dicMesas->find(k_mesita));
                    if(mesas == nullptr){
                        std::invalid_argument("La mesa no existe")
                    }

                 dicMesasVenta->add(k_mesita, mesita);   
               

         }

         void ControladorSistema::agregarVentaLocal(int numeroMesa){

         }

         void ControladorSistema::aplicarDescuento (int descuento){

         }

         DtFactura ControladorSistema::obtenerFactura (int codigoVenta){

         }

        void ControladorSistema::agregarProductoMenu(std::string codigo, int cantidad ){

         }

         void ControladorSistema::listarProductosComunes()
         {
            if(dicProductosU->getSize() == 0){
                throw std::invalid_argument(" No hay productos unitarios en el sistema");
            }
           ProductoUnitario::listarProductosComunes(dicProductosU);
         }
         

         void ControladorSistema::listarProductoMenu()
         {
            if(dicMenus->getSize()== 0){
                throw std::invalid_argument(" No hay Menus agregados");
            }
            Menu::ListarMenuses(dicMenus);
         }


        DtProducto ControladorSistema::seleccionarProducto (std::string id, std::string cantidad){
            //Los productos unitarios se los pasa a contiene la cantidad de productos
            // Manda un DtProducto a contiene con la cantidad;


         }

        void ControladorSistema::agregarProductoComun(std::string id, std::string descripcion, float precio){
            //Agrego al diccionario de productos del sistema
            StringKey *k_id = new StringKey(id);
            dicProductosU->add(k_id, descripcion, precio);
        }

        void ControladorSistema::altaProducto(std::string codigo, std::string desc, float precio){
        //Poder elegir entre producto menu y unitario
        // Solo se muestra menu si ya existen productos unitarios
        //ProdU pide codigo, descripcion y precio. Confirmar y cancelar.
        //Menu pide cod y descripcion, listar prodU por id, el admin elige cantidad
        //Opcion de seguir agregando productos.

        //StringKey *k_prod = new StringKey(prodEnSis);
        //Producto* prodsEnSis = dynamic_cast<Producto*>(dicProductosU->) 
           

            }//llave fin alta producto

        bool hayProductos(){

            IIterator *prSis = dicProductosU->getIterator();
            bool hayProductos;
        
            if(*prSis->hasCurrent()){

                hayProductos= true;

            }else{

                hayProductos = false;
            }
        return hayProductos;

        }// llave fin hay productos

        
        
        // >>>>>>>>> SECCION ELIMINATORIAS <<<<<<<<< //

        //dicProductos
        //dicVentas
        //

        //Elimino el Producto {codigo} de una venta {venta}. Pag 16.
        //Si cantProducto - cant > 0, decremento, y si es = 0, borra.
        void ControladorSistema::eliminarProductoVenta(std::string codigoP, int cant, std::string codigoV){
            //Claves
            StringKey* k_venta = new StringKey(codigoV);
            StringKey* k_producto = new StringKey(codigoP);
			//Objetos
            Venta* aux_venta = dynamic_cast<Venta*>dicVentas->find(k_venta);
                if(aux_venta){
                    throw std::invalid_argument("No existe Venta con ese codigo, intente nuevamente");
                }

            //Busco en la coleccion de {Transacciones} que tiene {aux_venta} buscando donde esta el {Producto} 
            //De la venta, saco la coleccion de transacciones
            ICollection* colTrans = aux_venta->getTransacciones();
            //Iterador para buscar dentro de las transacciones de la venta
            IIterator* itTrans = colTrans->getIterator();
            
            //flag
            
            //mientras haya transacciones que revisar  en {colTrans} y no este {found}
            while(itTrans->hasCurrent() and !(found)){
                //Una transaccion
                Transaccion* transacVenta = dynamic_cast<Transaccion*>itTrans->getCurrent();
                
                //Saco el dictionary de productos de {transacVenta}
                IDictionary* prod_tran = transacVenta->getProdTransaccion();
               
				bool found = false;
                //Existe el producto en esa transaccion ???
                found = prod_tran->member(k_producto)


                if(found) {
                	//Consigo el producto a removar de codigo {k_producto}
                	ICollectible* prod_a_remover = prod_tran->find(k_producto);
                    //Consigo el diccionaario de cantidades de la transaccion en tratamiento
                    IDictionary* cant_prod_tran = transacVenta->getCantTransaccion();
                    //Obtengo las cantidades de ese producto de forma ICollectible
                    ICollectible* cantidad_k_Prod = cant_prod_tran->find(k_producto);
                    //tengo la cantidad, hago el calculin
                    int cantidadResolucion = (dynamic_cast<int>cantidad_k_Prod->getValue()) - cant;
                    

                    //remuevo sin importar la resolucion
                    prod_tran->remove(prod_a_remover);
                    cant_prod_tran->remove(cantidad_k_Prod);

                    //Si el resultado es mayor que 0, se necesita tenerlo en la transaccion aun
                    if (cantidadResolucion>0)
                    {
                        Producto* aux_prod = dynamic_cast<Producto*>dicProductosU->find(k_producto);
                        transacVenta->addProducto(aux_prod,new Integer(cantidadResolucion));
                    }else if(prod_tran->isEmpty()){
                    	//Si prod_tran es Empty, es porque era el unico producto de la transaccion y eliminaremos la misma.
                       	
                       	//Delete a los dos diccionarios de la transaccion
                        delete prod_tran;
                        delete cant_prod_tran;
                        //Remuevo la transaccion de la venta
						colTrans->remove(transacVenta);
						//Delete3
                        delete transacVenta
                    }

                }
                itTrans->next();
            }
            delete k_producto;
            delete k_venta;
        }

        //Se quita el Producto del sistema, y se indica el tipo, si menu o prodUnitario
        void ControladorSistema::darBajaProducto(std::string codigo, std::string tipo){
        	StringKey* k_producto = new StringKey(codigo);

			ProductoUnitario* prodU = dynamic_cast<ProductoUnitario*> dicProductosU->find(k_producto);
			dicProductosU->remove(k_producto);

			delete prodU;
        	delete k_producto;

        }
        //Elimina un Menu del sistema.
        void ControladorSistema::darBajaMenu(std::string codigo, string nombre){
        		//Busco Key
        		StringKey* k_producto = new StringKey(codigo);

        		Menu* menu = dynamic_cast<Menu*> dicMenus->find(k_producto);
        	  	//Tengo que buscar las cosas que tiene el menu
        	  	Contiene* infoMenu = menu->getContiene();

        	  	//Colleciones a borrar
        	  	IDictionary* menuProdContiene = menu->getProdContiene();
        	  	IDictionary* menuCantContiene = menu->getCantContiene();
        	  	//Iteradores
        	  	IIterator* itProdContiene = menuProdContiene->getIterator();
        	  	IIterator* itCantContiene = menuCantContiene->getIterator();

        	  	//
        	  	while(!(itProdContiene->hasCurrent()) && !(itCantContiene->hasCurrent())) {

        	  		//Obtengo el producto y su cantidad de sus dic.
        	  		ICollectible* prodContiene = itProdContiene->getCurrent();
					ICollectible* prodCantContiene = itCantContiene->getCurrent();
					//Los remuevo
					menuProdContiene->remove(prodContiene);
					menuCantContiene->remove(prodCantContiene);
					//Deleteo instancia
					delete prodContiene;
					delete prodCantContiene;

					//neeeeeeeext
					itProdContiene->next();
					itCantContiene->next();      	  		
        	  	}
        	  	//remove n' delete Menu.
        	  	dicMenus->remove(menu);
        	  	delete menu;

        	  	delete k_producto;
        }

        //Modifica la cantidad un {Producto} de un {menu}, si la resta de Cantidad del producto y {cant} es 0, se borra.
        void ControladorSistema::modificarProductoMenu(std::string codigoP, int cant, std::string codigoM){

        	//Levanto KEY
        	StringKey* k_menu = new StringKey(codigoM);
        	StringKey* k_producto = new StringKey(codigoP);

        	Menu* menu = dynamic_cast<Menu*> dicMenus->find(k_menu);
        	//Tengo que buscar las cosas que tiene el menu
        	Contiene* infoMenu = menu->getContiene();

        	//Colleciones a borrar
        	IDictionary* menuProdContiene = menu->getProdContiene();
        	IDictionary* menuCantContiene = menu->getCantContiene();

        	//Collectibles
        	ProductoUnitario* prodContiene = dynamic_cast<ProductoUnitario*>menuProdContiene->find(k_producto);
        	int prodCantContiene = dynamic_cast<int>menuCantContiene->find(k_producto);

        	int cantidadResolucion = prodCantContiene - cant;

        	menuProdContiene->remove(prodContiene);
        	menuCantContiene->remove(prodCantContiene);

        	if (cantidadResolucion>0)
            {
            	//Lo vuelvo a agregar {Menu->Contiene}
                ProductoUnitario* aux_prod = dynamic_cast<ProductoUnitario*>dicProductosU->find(k_producto);
                infoMenu->addProducto(aux_prod,cantidadResolucion);
            }else {
            	delete prodContiene;
                delete prodCantContiene;
            }

            delete k_producto;
            delete k_menu;
        }	
