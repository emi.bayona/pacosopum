#include "../headers/ProductoUnitario.h"

// -------------------------- Constructor --------------------------//
ProductoUnitario::ProductoUnitario(string codigo, string descripcion, float precio):Producto(codigo, descripcion, precio)
{
	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;
	
}

void ProductoUnitario::listarProductosUnitarios(IDictionary *dicProductosU){

 IIterator *itera = dicProductosU->getIterator();
    while (itera->hasCurrent())
    {
      
      /* lineas de code 
      ProductoUnitario *currentProducto = dynamic_cast<ProductoUnitario *>(itera->getCurrent());
      std::cout<<"   "<<new DtProducto(currentProducto->getCodigo(), currentProducto->getDescripcion(), currentProducto->getPrecio());
      */

      //NO COMPILABA, LO COMENTE PARA AVISARTE
      std::cout<<"ERROR EN PRODUCTOUNITARIO.CPP, NO COMPILABA PREVIO"<<std::endl;
      

        itera->next();
            }

  delete itera;
}

// ------ Destructor -------//
ProductoUnitario::~ProductoUnitario()
{
	
};