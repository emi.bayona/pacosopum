#include "../headers/Venta.h"

// -------------------------- Constructor --------------------------//
Venta::Venta(int numero, float total, float subtotal, int descuento)
{
	this->numero=numero;
	this->total=total;
	this->subtotal=subtotal;
	this->descuento=descuento;
	this->transacciones = new List();
	
}

// ------ Getters ---------//
int Venta::getNumero()
{
	return this->numero;
}

float Venta::getTotal()
{
	return this->total;
}

float Venta::getSubtotal()
{
	return this->subtotal;
}
int Venta::getDescuento()
{
	return this->descuento;
}

ICollection* Venta::getTransacciones(){
	return this->transacciones;
}

// ---------------- Setters --------------- //
void Venta::setNumero(int numero)
{
	this->numero=numero;
}

void Venta::setTotal(float total)
{
	this->total=total;
}
void Venta::setSubtotal(float subtotal)
{
	this->subtotal=subtotal;
}
void Venta::setDescuento(int descuento)
{
	this->descuento=descuento;
}

void Venta::addTransacciones(ICollection* tran){

	IIterator* it = tran->getIterator();

	if (!it->hasCurrent()){
		throw std::invalid_argument("No hay transacciones para agregar");
	}

	while (it->hasCurrent()){
		//Transaccion* transac = dynamic_cast<Transaccion*>(it->getCurrent());
		//transacciones->add(transac);
		this->transacciones->add(it->getCurrent());
		it->next();
	}
}

void Venta::addTransaccion(ICollectible *tran){
	this->transacciones->add(tran);
}

// ------ Destructor -------//
Venta::~Venta()
{
	
};
