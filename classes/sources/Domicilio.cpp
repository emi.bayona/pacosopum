#include "../headers/Domicilio.h"

// -------------------------- Constructor --------------------------//
Domicilio::Domicilio(int numero, float total, float subtotal, int descuento):Venta(numero,total,subtotal,descuento)
{
	this->numero=numero;
	this->total=total;
	this->subtotal=subtotal;
	this->descuento=descuento;
	
}

// ------ Destructor -------//
Domicilio::~Domicilio()
{
	
};
