#include "../headers/Repartidor.h"

// -------------------------- Constructor --------------------------//
Repartidor::Repartidor(int numero, string nombre, TRAN transporte):Funcionario(numero,nombre)
{
	this->numero=numero;
	this->nombre=nombre;
	this->transporte=transporte;

}

// ------ Getters ---------//
Repartidor::TRAN Repartidor::getTransporte()
{
	return this->transporte;
}



// ---------------- Setters --------------- //
void Repartidor::setTransporte(TRAN transporte)
{
	this->transporte=transporte;
}



// ------ Destructor -------//
Repartidor::~Repartidor()
{
	
};
