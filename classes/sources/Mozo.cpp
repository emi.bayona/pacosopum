#include "../headers/Mozo.h"
#include <stdexcept>

// -------------------------- Constructor --------------------------//
Mozo::Mozo(int numero, std::string nombre):Funcionario(numero,nombre)
{
	this->numero=numero;
	this->nombre=nombre;
	this->dicMesas= new OrderedDictionary();
	
}

int Mozo::getNumero(){
	return this->numero;
}

std::string Mozo::getNombre(){
	return this->nombre;
}

IDictionary* Mozo::getMesas(){

	return this->dicMesas;
}

void Mozo::setNumero(int numero){
	this->numero=numero;
}

void Mozo::setNombre(std::string nombre ){
	this->nombre=nombre;
}

void Mozo::addMesa(Mesa *m){
	//genero la key del dic
	IntKey* intK = new IntKey(m->getNumero());
	//agrego al dic
	dicMesas->add(intK,m);
}

// ------ Destructor -------//
Mozo::~Mozo()
{
	
};
