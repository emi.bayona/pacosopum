#include "../headers/Transaccion.h"
#include "../headers/Producto.h"

// -------------------------- Constructor --------------------------//
Transaccion::Transaccion()
{
	this->productos = new OrderedDictionary();
	this->cantProductos = new OrderedDictionary();
}


// ------ Getters ---------//
IDictionary* Transaccion::getProdTransaccion(){
	return this->productos;
}
IDictionary* Transaccion::getCantTransaccion(){
	return this->cantProductos;

}


void Transaccion::addProductosTransaccion(ICollection* cants, IDictionary* prod){

	//Get iterators de las Collection
	IIterator* itCant = cants->getIterator();
	IIterator* itProd = prod->getIterator();

	if(!itCant->hasCurrent()){
		throw std::invalid_argument("No se agregaron cantidades al menu");
	}else if(!itProd->hasCurrent()){
		throw std::invalid_argument("No se agregaron productos al Menu");	
	}

	while((itProd->hasCurrent()) && (itCant->hasCurrent())){

		//hago el producto
		Producto* prodU = dynamic_cast<Producto*> (itCant->getCurrent());
		//le hago su {*k}
		StringKey* kp = new StringKey(prodU->getCodigo());
        StringKey* kcant = new StringKey(prodU->getCodigo());
		
		//agreggo el producto de {*k}
		productos->add(kp,prodU);
		//agrego la cantidad de la {*k}
		cantProductos->add(kcant,itProd->getCurrent());

		//Avanzo
		itProd->next();
		itCant->next();
	}
	// itCant->delete();
	// itProd->delete();
	// k->delete();
}

void Transaccion::addProducto(Producto* prod, int cant){
	/*
	IIterator* itProdU = prod->getIterator();

	if(!itProdU->hasCurrent()){
		throw std::invalid_argument("No se agregaron productos");	
	}

	while(itProdU->hasCurrent()){

		Producto* prod = dynamic_cast<Producto*> (itProdU->getCurrent());

		StringKey *k = new StringKey(prod->getCodigo());

		productos->add(k,prod)

		itProdU->next();
	}
	*/

	//el control del throw hacerlo aca o antes

	StringKey *k_prod = new StringKey(prod->getCodigo());
	StringKey *k_cant = new StringKey(prod->getCodigo());

	Integer* intu = new Integer(cant);

	this->productos->add(k_prod,prod);
	this->cantProductos->add(k_cant,intu);

}

// ------ Destructor -------//
Transaccion::~Transaccion()
{
	
};