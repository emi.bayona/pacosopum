#include "../headers/Mesa.h"

// -------------------------- Constructor --------------------------//
Mesa::Mesa(int numero)
{
	this->numero=numero;
	
}


// ------ Getters ---------//
int Mesa::getNumero()
{
	return this->numero;
}



// ---------------- Setters --------------- //
void Mesa::setNumero(int numero)
{
	this->numero=numero;
}

// ------ Destructor -------//
Mesa::~Mesa()
{
	
};
