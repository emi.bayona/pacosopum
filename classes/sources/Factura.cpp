#include "../headers/Factura.h"

// -------------------------- Constructor --------------------------//
Factura::Factura(int codigoVenta, DtFecha fecha, DtHora hora):fecha(fecha),hora(hora)
{
	this->codigoVenta=codigoVenta;
	this->fecha=fecha;
	this->hora=hora;
	
}


// ------ Getters ---------//
int Factura::getCodigoVenta()
{
	return this->codigoVenta;
}

DtFecha Factura::getFecha()
{
	return this->fecha;
}

DtHora Factura::getHora()
{
	return this->hora;
}



// ---------------- Setters --------------- //
void Factura::setCodigoVenta(int codigoVenta)
{
	this->codigoVenta=codigoVenta;
}

void Factura::setFecha(DtFecha fecha)
{
	this->fecha=fecha;
}
void Factura::setHora(DtHora hora)
{
	this->hora=hora;
}

// ------ Destructor -------//
Factura::~Factura()
{
	
};
