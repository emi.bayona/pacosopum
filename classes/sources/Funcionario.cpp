#include "../headers/Funcionario.h"

// -------------------------- Constructor --------------------------//
Funcionario::Funcionario(int numero, string nombre)
{
	this->numero=numero;
	this->nombre=nombre;
	
}


// ------ Getters ---------//
int Funcionario::getNumero()
{
	return this->numero;
}

std::string Funcionario::getNombre()
{
	return this->nombre;
}

// ---------------- Setters --------------- //
void Funcionario::setNumero(int numero)
{
	this->numero=numero;
}

void Funcionario::setNombre(string nombre)
{
	this->nombre=nombre;
}

// ------ Destructor -------//
Funcionario::~Funcionario()
{
	
};
