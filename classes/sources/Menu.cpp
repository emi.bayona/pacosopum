#include "../headers/Menu.h"

// -------------------------- Constructor --------------------------//
Menu::Menu(std::string codigo, std::string descripcion, float precio, std::string nombre):Producto(codigo, descripcion, precio)
{
	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;
	this->nombre=nombre;
}


// ------ Getters ---------//
string Menu::getNombre()
{
	return this->nombre;
}

Contiene* Menu::getContiene(){
	return this->contiene;
}

// ---------------- Setters --------------- //
void Menu::setNombre(string nombre)
{
	this->nombre=nombre;
}

void Menu::setContiene(Contiene* contiene){
	this->contiene=contiene;
}

void Menu::listarMenuses(IDictionary* dicMenus){

	IIterator *iteraM = dicMenus->getIterator();
        while (iteraM->hasCurrent())
        {
           Menu *currentMenu = dynamic_cast<Menu *>(iteraM->getCurrent());
           
           //Nope.
           //std::cout<<"   "<<new DtMenu(currentMenu->getNombre()); //, currentMenu->getDescripcion(), currentMenu->getPrecio(), currentMenu->getCantidad());
          
           std::cout<<"   "<<(currentMenu->getNombre()); //, currentMenu->getDescripcion(), currentMenu->getPrecio(), currentMenu->getCantidad());
           

           Contiene* productosContenidosMenu = currentMenu->getContiene();
  
          //Le mandabas un Contiene y no un IDictionary         
           ProductoUnitario::listarProductosUnitarios(productosContenidosMenu->getProdContiene());


    iteraM->next();

        }

   delete iteraM;


}

// ------- Destructor -------//
Menu::~Menu()
{
	
};
