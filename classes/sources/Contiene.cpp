#include "../headers/Contiene.h"
//#include "../../classes/headers/Producto.h"

//Contiene es quien sabe cuantos productos unitarios tiene y su precio
// -------------------------- Constructor --------------------------//
// Contiene::Contiene(int cantidad,ProductoUnitario* prodU)
// {
// 	this->cantidad=cantidad;
// 	this->menu = new Menu*[MENUS];
// 	this->productoUnitario = new ProductoUnitario*[PRODUCTOS];

// }

Contiene::Contiene(){
	this->productos = new OrderedDictionary();
	this->cantProductos = new OrderedDictionary();
}

// ------ Getters ---------//
IDictionary* Contiene::getProdContiene(){
	return this->productos;
}
IDictionary* Contiene::getCantContiene(){
	return this->cantProductos;

}

// ---------------- Setters --------------- //

//Agrega una lista de productos y sus cantidades al menu???Contiene.
void Contiene::addProductosContiene(ICollection* cants, IDictionary* prodUni){

	//Get iterators de las Collection
	IIterator* itCant = cants->getIterator(); // Collecion de INTEGER
	IIterator* itProd = prodUni->getIterator(); //DICTIONARY DE PRODUCTOS Unitarios

	if(!itCant->hasCurrent()){
		throw std::invalid_argument("No se agregaron cantidades al menu");
	}else if(!itProd->hasCurrent()){
		throw std::invalid_argument("No se agregaron productos al Menu");	
	}

	while((itProd->hasCurrent()) && (itCant->hasCurrent())){

		//hago el producto
		Producto* prodU = dynamic_cast<Producto*> (itCant->getCurrent());
		//le hago su {*k}
		StringKey *k = new StringKey(prodU->getCodigo());

		//agreggo el producto de {*k}
		productos->add(k,prodU);
		//agrego la cantidad de la {*k}
		cantProductos->add(k,itCant->getCurrent());

		//Avanzo
		itProd->next();
		itCant->next();
	}
	// itCant->delete();
	// itProd->delete();
	// k->delete();
}

void Contiene::addProducto(Producto* prod, int cant){
	
	/*IIterator* itProdU = prod->getIterator();

	if(!itProdU->hasCurrent()){
		throw std::invalid_argument("No se agregaron productos");	
	}

	while(itProdU->hasCurrent()){

		ProductoUnitario* prod = dynamic_cast<ProductoUnitario*> (itProdU->getCurrent());

		StringKey *k = new StringKey(prod->getCodigo());

		productosUnitarios->add(k,prod)

		itProdU->next();
	}*/
}

// ------ Destructor -------//
Contiene::~Contiene(){
	
};





















//Objetivo: agregar {prod} con su {cant} en los Dictionary correspondientes utilizando su {key=codigo}
