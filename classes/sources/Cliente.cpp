#include "../headers/Cliente.h"
#include "../headers/Domicilio.h"


// -------------------------- Constructor --------------------------//
Cliente::Cliente(string nombre, int telefono, string direccion)
{
	this->nombre=nombre;
	this->telefono=telefono;
	this->direccion=direccion;	
}

// ------ Getters ---------//
string Cliente::getNombre()
{
	return this->nombre;
}

int Cliente::getTelefono()
{
	return this->telefono;
}

string Cliente::getDireccion()
{
	return this->direccion;
}


// ---------------- Setters --------------- //
void Cliente::setNombre(string nombre)
{
	this->nombre=nombre;
}

void Cliente::setTelefono(int telefono)
{
	this->telefono=telefono;
}
void Cliente::setDireccion(string direccion)
{
	this->direccion=direccion;
}

// ------ Destructor -------//
Cliente::~Cliente()
{
	
};
