#include "../headers/Producto.h"

// -------------------------- Constructor --------------------------//
Producto::Producto(string codigo, string descripcion, float precio)
{
	this->codigo=codigo;
	this->descripcion=descripcion;
	this->precio=precio;

	
}

// ------ Getters ---------//
string Producto::getCodigo()
{
	return this->codigo;
}

string Producto::getDescripcion()
{
	return this->descripcion;
}

float Producto::getPrecio()
{
	return this->precio;
}

// ---------------- Setters --------------- //
void Producto::setCodigo(string codigo)
{
	this->codigo=codigo;
}

void Producto::setDescripcion(string descripcion)
{
	this->descripcion=descripcion;
}
void Producto::setPrecio(float precio)
{
	this->precio=precio;
}

// ------ Destructor -------//
Producto::~Producto()
{
	
};
