OBJETOS = ICollectible.o IKey.o OrderedKey.o IIterator.o ICollection.o \
ListNode.o ListIterator.o List.o IDictionary.o OrderedDictionaryEntry.o \
OrderedDictionary.o String.o StringKey.o IntKey.o \
Cliente.o Contiene.o \
Domicilio.o Factura.o Funcionario.o \
Local.o Menu.o Mesa.o \
Mozo.o Producto.o ProductoUnitario.o \
Repartidor.o Transaccion.o Venta.o \
DtCliente.o DtDomicilio.o \
DtFactura.o DtFecha.o \
DtFuncionario.o DtLocal.o \
DtMenu.o DtMesa.o DtMozo.o \
DtProducto.o DtProductoUnitario.o \
DtRepartidor.o  DtVenta.o \
ControladorSistema.o Fabrica.o ISistema.o

FUENTES = ICollection/interfaces/ICollectible.cpp ICollection/interfaces/ICollectible.h \
ICollection/interfaces/IKey.cpp ICollection/interfaces/IKey.h \
ICollection/interfaces/OrderedKey.cpp ICollection/interfaces/OrderedKey.h \
ICollection/String.cpp ICollection/String.h \
ICollection/StringKey.cpp ICollection/StringKey.h \
ICollection/Integer.cpp ICollection/Integer.h \
ICollection/IntKey.cpp ICollection/IntKey.h \
ICollection/interfaces/IIterator.cpp ICollection/interfaces/IIterator.h \
ICollection/interfaces/ICollection.cpp ICollection/interfaces/ICollection.h \
ICollection/collections/ListNode.cpp ICollection/collections/ListNode.h \
ICollection/collections/ListIterator.cpp ICollection/collections/ListIterator.h \
ICollection/collections/List.cpp ICollection/collections/List.h \
ICollection/interfaces/IDictionary.cpp ICollection/interfaces/IDictionary.h \
ICollection/collections/OrderedDictionaryEntry.cpp ICollection/collections/OrderedDictionaryEntry.h \
ICollection/collections/OrderedDictionary.cpp ICollection/collections/OrderedDictionary.h \
ICollection/String.cpp ICollection/String.h \
IColection/StringKey.cpp ICollection/StringKey.h \
IColection/IntKey.cpp ICollection/IntKey.h \
Cliente.cpp Cliente.h \
Contiene.cpp Contiene.h \
Domicilio.cpp Domicilio.h \
Factura.cpp Factura.h \
Funcionario.cpp Funcionario.h \
Local.cpp Local.h \
Menu.cpp Menu.h \
Mesa.cpp Mesa.h \
Mozo.cpp Mozo.h \
Producto.cpp Producto.h \
ProductoUnitario.cpp ProductoUnitario.h \
Repartidor.cpp Repartidor.h \
Transaccion.cpp Transaccion.h \
Venta.cpp Venta.h \
DtCliente.cpp DtCliente.h \
DtDomicilio.cpp DtDomicilio.h \
DtFactura.cpp DtFactura.h \
DtFecha.cpp DtFecha.h \
DtFuncionario.cpp DtFuncionario.h \
DtLocal.cpp DtLocal.h \
DtMenu.cpp DtMenu.h \
DtMesa.cpp DtMesa.h \
DtMozo.cpp DtMozo.h \
DtProducto.cpp DtProducto.h \
DtProductoUnitario.cpp DtProductoUnitario.h \
DtRepartidor.cpp DtRepartidor.h \
DtVenta.cpp DtVenta.h \
classes/interfaces/ControladorSistema.cpp classes/interfaces/ControladorSistema.h \
classes/interfaces/Fabrica.cpp classes/interfaces/Fabirca.h \
classes/interfaces/ISistema.cpp classes/interfaces/ISistema.h

CC = g++
OPCIONES = -std=c++11 -g -Wall -Werror

main: $(OBJETOS) main.cpp Makefile
	$(CC) $(OPCIONES) $(OBJETOS) main.cpp -o main

ICollectible.o: ICollection/interfaces/ICollectible.h ICollection/interfaces/ICollectible.cpp
	$(CC) $(OPCIONES) -c ICollection/interfaces/ICollectible.cpp -o ICollectible.o

IKey.o: ICollection/interfaces/IKey.h ICollection/interfaces/IKey.cpp
	$(CC) $(OPCIONES) -c ICollection/interfaces/IKey.cpp -o IKey.o

OrderedKey.o: ICollection/interfaces/OrderedKey.h ICollection/interfaces/OrderedKey.cpp IKey.o
	$(CC) $(OPCIONES) -c ICollection/interfaces/OrderedKey.cpp -o OrderedKey.o

IIterator.o: ICollection/interfaces/IIterator.h ICollection/interfaces/IIterator.cpp ICollectible.o
	$(CC) $(OPCIONES) -c ICollection/interfaces/IIterator.cpp -o IIterator.o

ICollection.o: ICollection/interfaces/ICollection.h ICollection/interfaces/ICollection.cpp IIterator.o
	$(CC) $(OPCIONES) -c ICollection/interfaces/ICollection.cpp -o ICollection.o

ListNode.o: ICollection/collections/ListNode.h ICollection/collections/ListNode.cpp ICollection.o
	$(CC) $(OPCIONES) -c ICollection/collections/ListNode.cpp -o ListNode.o

ListIterator.o: ICollection/collections/ListIterator.h ICollection/collections/ListIterator.cpp ListNode.o
	$(CC) $(OPCIONES) -c ICollection/collections/ListIterator.cpp -o ListIterator.o

List.o: ICollection/collections/List.h ICollection/collections/List.cpp ListNode.o ListIterator.o
	$(CC) $(OPCIONES) -c ICollection/collections/List.cpp -o List.o

IDictionary.o: ICollection/interfaces/IDictionary.h ICollection/interfaces/IDictionary.cpp IKey.o ICollectible.o IIterator.o
	$(CC) $(OPCIONES) -c ICollection/interfaces/IDictionary.cpp -o IDictionary.o

OrderedDictionaryEntry.o: ICollection/collections/OrderedDictionaryEntry.h ICollection/collections/OrderedDictionaryEntry.cpp OrderedKey.o ICollectible.o
	$(CC) $(OPCIONES) -c ICollection/collections/OrderedDictionaryEntry.cpp -o OrderedDictionaryEntry.o

OrderedDictionary.o: ICollection/collections/OrderedDictionary.h ICollection/collections/OrderedDictionary.cpp OrderedDictionaryEntry.o ListNode.o IDictionary.o ListIterator.o
	$(CC) $(OPCIONES) -c ICollection/collections/OrderedDictionary.cpp -o OrderedDictionary.o

String.o: ICollection/String.h ICollection/String.cpp
	$(CC) $(OPCIONES) -c ICollection/String.cpp -o String.o

StringKey.o: ICollection/StringKey.h ICollection/StringKey.cpp
	$(CC) $(OPCIONES) -c ICollection/StringKey.cpp -o StringKey.o
	
IntKey.o: ICollection/IntKey.h ICollection/IntKey.cpp
	$(CC) $(OPCIONES) -c ICollection/IntKey.cpp -o IntKey.o	

Cliente.o: ./classes/headers/Cliente.h ./classes/sources/Cliente.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Cliente.cpp

Contiene.o: ./classes/headers/Contiene.h ./classes/sources/Contiene.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Contiene.cpp

Domicilio.o: ./classes/headers/Domicilio.h ./classes/sources/Domicilio.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Domicilio.cpp

Factura.o: ./classes/headers/Factura.h ./classes/sources/Factura.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Factura.cpp

Funcionario.o: ./classes/headers/Funcionario.h ./classes/sources/Funcionario.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Funcionario.cpp

Local.o: ./classes/headers/Local.h ./classes/sources/Local.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Local.cpp

Menu.o: ./classes/headers/Menu.h ./classes/sources/Menu.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Menu.cpp

Mesa.o: ./classes/headers/Mesa.h ./classes/sources/Mesa.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Mesa.cpp

Mozo.o: ./classes/headers/Mozo.h ./classes/sources/Mozo.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Mozo.cpp

Producto.o: ./classes/headers/Producto.h ./classes/sources/Producto.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Producto.cpp

ProductoUnitario.o: ./classes/headers/ProductoUnitario.h ./classes/sources/ProductoUnitario.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/ProductoUnitario.cpp

Repartidor.o: ./classes/headers/Repartidor.h ./classes/sources/Repartidor.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Repartidor.cpp

Transaccion.o: ./classes/headers/Transaccion.h ./classes/sources/Transaccion.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Transaccion.cpp

Venta.o: ./classes/headers/Venta.h ./classes/sources/Venta.cpp
	$(CC) $(OPCIONES) -c ./classes/sources/Venta.cpp

DtCliente.o: ./datatypes/headers/DtCliente.h ./datatypes/sources/DtCliente.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtCliente.cpp

DtDomicilio.o: ./datatypes/headers/DtDomicilio.h ./datatypes/sources/DtDomicilio.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtDomicilio.cpp

DtFactura.o: ./datatypes/headers/DtFactura.h ./datatypes/sources/DtFactura.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtFactura.cpp

DtFecha.o: ./datatypes/headers/DtFecha.h ./datatypes/sources/DtFecha.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtFecha.cpp

DtFuncionario.o: ./datatypes/headers/DtFuncionario.h ./datatypes/sources/DtFuncionario.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtFuncionario.cpp

DtLocal.o: ./datatypes/headers/DtLocal.h ./datatypes/sources/DtLocal.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtLocal.cpp

DtMenu.o: ./datatypes/headers/DtMenu.h ./datatypes/sources/DtMenu.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtMenu.cpp

DtMesa.o: ./datatypes/headers/DtMesa.h ./datatypes/sources/DtMesa.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtMesa.cpp

DtMozo.o: ./datatypes/headers/DtMozo.h ./datatypes/sources/DtMozo.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtMozo.cpp

DtProducto.o: ./datatypes/headers/DtProducto.h ./datatypes/sources/DtProducto.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtProducto.cpp

DtProductoUnitario.o: ./datatypes/headers/DtProductoUnitario.h ./datatypes/sources/DtProductoUnitario.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtProductoUnitario.cpp

DtRepartidor.o: ./datatypes/headers/DtRepartidor.h ./datatypes/sources/DtRepartidor.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtRepartidor.cpp

DtVenta.o: ./datatypes/headers/DtVenta.h ./datatypes/sources/DtVenta.cpp
	$(CC) $(OPCIONES) -c ./datatypes/sources/DtVenta.cpp

ControladorSistema.o: classes/interfaces/ControladorSistema.cpp classes/interfaces/ControladorSistema.h
	$(CC) $(OPCIONES) -c classes/interfaces/ControladorSistema.cpp -o ControladorSistema.o

Fabrica.o: classes/interfaces/Fabrica.cpp classes/interfaces/Fabrica.h
	$(CC) $(OPCIONES) -c classes/interfaces/Fabrica.cpp -o Fabrica.o

ISistema.o: classes/interfaces/ISistema.cpp classes/interfaces/ISistema.h
	$(CC) $(OPCIONES) -c classes/interfaces/ISistema.cpp -o ISistema.o

clean:
	rm -f $(OBJETOS) main

rebuild:
	make clean
	make

